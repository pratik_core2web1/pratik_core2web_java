public class MaximumSubarraySum {
    public static void main(String[] args) {
        int[] nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        int maxSum = maxSubArray(nums);
        System.out.println("The maximum sum of a subarray is: " + maxSum);
    }

    public static int maxSubArray(int[] nums) {
        int maxSoFar = nums[0];  // Initialize maxSoFar to the first element
        int currentMax = nums[0]; // Initialize currentMax to the first element

        // Iterate through the array starting from the second element
        for (int i = 1; i < nums.length; i++) {
            currentMax = Math.max(nums[i], currentMax + nums[i]); // Update currentMax
            maxSoFar = Math.max(maxSoFar, currentMax); // Update maxSoFar if currentMax is greater
        }

        return maxSoFar;
    }
}

