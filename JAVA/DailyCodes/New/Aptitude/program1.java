//for Loop
class Main {
    public static void main(String[] args) {
        int num = 5678;
        int Reverse = 0;
        System.out.println(num);

        for (; num != 0; num /= 10) {
            int digit = num % 10;
            Reverse = Reverse * 10 + digit;
        }

        System.out.println(Reverse);
    }
}

// Scanner
/*import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int num = sc.nextInt();
        int Reverse = 0;

        System.out.println("Original number: " + num);

        for (; num != 0; num /= 10) {
            int digit = num % 10;
            Reverse = Reverse * 10 + digit;
        }

        System.out.println("Reversed number: " + Reverse);


    }
}
*/
                     
