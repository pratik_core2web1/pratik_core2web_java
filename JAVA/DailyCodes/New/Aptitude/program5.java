class FindTargetInArray {
    public static void main(String[] args) {
        int[] array = {2, 3, 4, 10, 40}; // Given array
        int target = 10; // Target element to find

        int index = findTarget(array, target);

        if (index != -1) {
            System.out.println("Target element " + target + " found at index: " + index);
        } else {
            System.out.println("Target element " + target + " not found in the array.");
        }
    }

    public static int findTarget(int[] array, int target) {
        // Loop through the array
        for (int i = 0; i < array.length; i++) {
            if (array[i] == target) {
                return i; // Return the index where the target is found
            }
        }
        return -1; // Return -1 if the target is not found
    }
}

