import java.util.Scanner;

class PrimeCheck {
    public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
        
	    System.out.print("Enter a number: ");
        int number = scanner.nextInt();
        
        if (isPrime(number)) {
            System.out.println(number + " is a prime number.");
        } else {
            System.out.println(number + " is not a prime number.");
      }  
}
 


       public static boolean isPrime(int num) {
 
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false; // If divisible by any i, num is not prime
            }
        }
        
        
	
	return true; // If no divisors were found, num is prime
    }
}

