public class LargestNumber {
    public static void main(String[] args) {
        int arr[] = {9, 1, 5, 7, 3, 4};
        int max = arr[0]; // Assume the first element is the largest

        // Iterate through the array
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i]; // Update max if the current element is greater
            }
        }

        // Output the largest number
        System.out.println("The largest number in the array is: " + max);
    }
}

