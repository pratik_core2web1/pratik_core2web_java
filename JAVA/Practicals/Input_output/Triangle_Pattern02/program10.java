import java.util.Scanner;

class InputOutput02 {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the number of rows
        System.out.print("Rows = ");
        int rows = scanner.nextInt();

        // Initialize the starting character
        char startChar = (char) (97 + rows - 1);

        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print the characters in each row
            for (char ch = startChar; ch >= 97; ch--) {
                System.out.print(ch + " ");
            }

            // Update the starting character for the next row
            startChar--;

            // Move to the next line after printing each row
            System.out.println();
        }

        // Close the Scanner object
        scanner.close();
    }
}

