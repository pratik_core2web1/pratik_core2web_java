import java.util.Scanner;

class InputOutput02 {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the number of rows
        System.out.print("Rows = ");
        int rows = scanner.nextInt();

        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print the numbers in each row
            for (int j = i; j <= rows; j++) {
                System.out.print(j + " ");
            }
            
            // Move to the next line after printing each row
            System.out.println();
        }

        // Close the Scanner object
        scanner.close();
    }
}

