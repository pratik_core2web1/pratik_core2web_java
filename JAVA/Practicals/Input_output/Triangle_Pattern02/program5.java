import java.util.Scanner;

class InputOutput02 {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the number of rows
        System.out.print("Rows = ");
        int rows = scanner.nextInt();

        // Initialize the starting character
        char startChar = 'A';

        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print the characters in each row
            for (int j = 1; j <= i; j++) {
                if (i % 2 == 0) {
                    System.out.print(Character.toLowerCase(startChar) + " ");
                } else {
                    System.out.print(startChar + " ");
                }
                startChar++;
            }

            // Move to the next line after printing each row
            System.out.println();
        }

        // Close the Scanner object
        scanner.close();
    }
}

