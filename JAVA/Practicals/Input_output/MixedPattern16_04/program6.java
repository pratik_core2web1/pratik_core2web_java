import java.util.Scanner;

class Inputoutput04{
    // Function to calculate factorial
    static int factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        scanner.close();

        char startChar = (char) ('a' + rows - 1); // Initial character for the first row
        for (int i = 0; i < rows; i++) {
            char currentChar = startChar;
            if (i % 2 == 0) { // If row index is even, print characters
                for (int j = 0; j <= i; j++) {
                    System.out.print(currentChar + " ");
                    currentChar--;
                }
            } else { // If row index is odd, print numbers
                for (int j = 0; j <= i; j++) {
                    System.out.print(factorial(rows - j) + " ");
                }
            }
            System.out.println();
        }
    }
}

