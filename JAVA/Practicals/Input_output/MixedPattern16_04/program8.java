import java.util.Scanner;

class Inputoutput04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        scanner.close();

        char startChar = (char) ('A' + rows - 1); // Initial character for the first row
        for (int i = 0; i < rows; i++) {
            char currentChar = startChar;
            for (int j = 0; j < rows - i; j++) {
                System.out.print(currentChar + " ");
                currentChar--;
            }
            System.out.println();
        }
    }
}

