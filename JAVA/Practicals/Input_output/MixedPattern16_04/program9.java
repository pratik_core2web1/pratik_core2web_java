import java.util.Scanner;

class Inputoutput04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        scanner.close();

        for (int i = 0; i < rows; i++) {
            if (i % 2 == 0) { // If row index is even, print numbers in increasing order
                for (int j = 1; j <= rows - i; j++) {
                    System.out.print(j + " ");
                }
            } else { // If row index is odd, print characters in descending order
                char currentChar = (char) ('A' + rows - i - 1);
                for (int j = 0; j < rows - i; j++) {
                    System.out.print(currentChar + " ");
                    currentChar--;
                }
            }
            System.out.println();
        }
    }
}

