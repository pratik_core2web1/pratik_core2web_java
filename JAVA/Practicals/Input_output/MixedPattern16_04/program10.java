import java.util.ArrayList;
import java.util.Scanner;

class Inputoutput04 {
    // Function to reverse a number
    static long reverseNumber(long num) {
        long reversedNum = 0;
        while (num != 0) {
            long digit = num % 10;
            reversedNum = reversedNum * 10 + digit;
            num /= 10;
        }
        return reversedNum;
    }

    // Function to print the square of odd digits
    static void printSquareOfOddDigits(long num) {
        ArrayList<Long> oddDigits = new ArrayList<>();
        while (num != 0) {
            long digit = num % 10;
            if (digit % 2 != 0) {
                oddDigits.add(digit * digit);
            }
            num /= 10;
        }
        // Print the square of odd digits
        for (int i = oddDigits.size() - 1; i >= 0; i--) {
            System.out.print(oddDigits.get(i));
            if (i != 0) {
                System.out.print(", ");
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        long num = scanner.nextLong();
        scanner.close();

        // Reverse the number
        long reversedNum = reverseNumber(num);
        // Print the square of odd digits
        printSquareOfOddDigits(reversedNum);
    }
}

