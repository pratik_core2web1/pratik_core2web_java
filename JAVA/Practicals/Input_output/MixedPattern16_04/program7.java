import java.util.Scanner;

class Inputoutput04 {
    // Function to calculate factorial
    static long factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        scanner.close();

        int startNum = 2; // Initial number for the first row
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows - i; j++) {
                System.out.print(startNum + " ");
                startNum += 2;
            }
            System.out.println();
        }
    }
}

