import java.util.Scanner;

class Inputoutput04{
      public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        scanner.close();

        char startChar = 'C'; // Initial character for the first row
        int startNum = 1; // Initial number for the second row

        for (int i = 0; i < rows; i++) {
            if (i % 2 == 0) { // If row index is even, print characters
                for (int j = 0; j < rows; j++) {
                    System.out.print((char) (startChar - j) + " ");
                }
            } else { // If row index is odd, print numbers
                for (int j = 0; j < rows; j++) {
                    System.out.print(startNum + " ");
                    startNum++;
                }
            }
            System.out.println();
        }
    }
}

