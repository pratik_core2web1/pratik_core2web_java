import java.util.Scanner;

class Inputoutput04 {
    // Function to check if a number is composite
    static boolean isComposite(int num) {
        if (num <= 1) {
            return false; // 1 and non-positive numbers are not considered composite
        }
        for (int i = 2; i * i <= num; i++) {
            if (num % i == 0) {
                return true; // If any divisor is found, it's composite
            }
        }
        return false; // If no divisors found, it's not composite
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        scanner.close();

        int currentNum = rows;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(currentNum + " ");
                currentNum--;
            }
            System.out.println();
        }
    }
}

