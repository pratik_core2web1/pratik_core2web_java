import java.util.Scanner;

class Inputoutput04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        scanner.close();

        int num = 1;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows; j++) {
                System.out.print(num + " ");
                num++;
            }
            System.out.println();
        }
    }
}

