import java.util.Scanner;

class InputOutput03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int num = scanner.nextInt();
        scanner.close();

        boolean isComposite = false;
        if (num <= 1) {
            isComposite = false; // By definition, 1 and negative numbers are not considered composite.
        } else {
            for (int i = 2; i <= Math.sqrt(num); i++) {
                if (num % i == 0) {
                    isComposite = true; // If any divisor is found, it's composite.
                    break;
                }
            }
        }

        if (isComposite) {
            System.out.println(num + " is a composite number.");
        } else {
            System.out.println(num + " is not a composite number.");
        }
    }
}

