import java.util.Scanner;

class InputOutput03{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        long num = scanner.nextLong();
        scanner.close();

        long reversedNum = 0;
        while (num != 0) {
            long digit = num % 10;
            reversedNum = reversedNum * 10 + digit;
            num /= 10;
        }

        System.out.println("Reverse of " + num + " is " + reversedNum + ".");
    }
}

