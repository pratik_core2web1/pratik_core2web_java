import java.util.Scanner;

class IO01  {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the number of rows
        System.out.print("rows=");
        int rows = scanner.nextInt();

        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print the numbers in each row
            for (int j = 1; j <= i; j++) {
                // Print the number "9" for rows 1 to 3 and "16" for rows 4 onwards
                System.out.print((rows < 4 ? "9" : "16") + " ");
            }
            // Move to the next line after printing each row
            System.out.println();
        }

        // Close the Scanner object
        scanner.close();
    }
}

