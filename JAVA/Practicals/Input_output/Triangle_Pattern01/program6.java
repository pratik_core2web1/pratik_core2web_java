import java.util.Scanner;

class InputOutput01{
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the number of rows
        System.out.print("rows=");
        int rows = scanner.nextInt();

        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print the characters in each row
            char startChar = (i % 2 == 0) ? 'B' : '1';
            char endChar = (i % 2 == 0) ? 'C' : (char) ('0' + i);

            for (char ch = startChar; ch <= endChar; ch++) {
                System.out.print(ch + " ");
            }

            // Move to the next line after printing each row
            System.out.println();
        }

        // Close the Scanner object
        scanner.close();
    }
}

