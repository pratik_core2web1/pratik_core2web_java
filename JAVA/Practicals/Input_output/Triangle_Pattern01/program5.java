import java.util.Scanner;

class InputOutput01 {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the number of rows
        System.out.print("rows=");
        int rows = scanner.nextInt();

        // Initialize the starting character
        char startChar = (char) ('D' - 1);

        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print the characters in each row
            for (int j = 1; j <= i; j++) {
                System.out.print((char) (startChar + j) + " ");
            }
            // Update the start character for the next row
            startChar += i + 1;

            // Move to the next line after printing each row
            System.out.println();
        }

        // Close the Scanner object
        scanner.close();
    }
}

