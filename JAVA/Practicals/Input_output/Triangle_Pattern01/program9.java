import java.util.Scanner;

class InputOutput01 {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the number of rows
        System.out.print("rows=");
        int rows = scanner.nextInt();

        // Initialize the starting number
        int startNumber = rows + 3;

        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print the characters/numbers in each row
            for (int j = 1; j <= i; j++) {
                if (j == 1) {
                    System.out.print(startNumber + " ");
                } else if (j % 2 == 0) {
                    System.out.print((char) ('a' + j - 2) + " ");
                } else {
                    System.out.print(startNumber + " ");
                }
            }
            
            if (i % 2 == 0) {
                startNumber += 2;
            }
            
            // Move to the next line after printing each row
            System.out.println();
        }

        // Close the Scanner object
        scanner.close();
    }
}

