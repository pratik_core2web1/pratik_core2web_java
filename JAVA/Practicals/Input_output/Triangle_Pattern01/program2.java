import java.util.Scanner;

class InputOutput01{
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the number of rows
        System.out.print("rows=");
        int rows = scanner.nextInt();

        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print the characters in each row
            for (int j = 1; j <= i; j++) {
                if (i % 2 != 0) {
                    // Print 'a' for odd rows
                    System.out.print("a ");
                } else {
                    // Print '$' for even rows
                    System.out.print("$ ");
                }
                if (i == rows) {
                    // Print characters from 'a' to 'a' + (rows - 1) for the last row
                    System.out.print((char) ('a' + j - 1) + " ");
                }
            }
            // Move to the next line after printing each row
            System.out.println();
        }

        // Close the Scanner object
        scanner.close();
    }
}

