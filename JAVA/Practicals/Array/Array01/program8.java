import java.util.Scanner;

class EmployeeAges {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Ask the user for the count of employees
        System.out.print("Enter the number of employees: ");
        int count = scanner.nextInt();

        // Define an array to store employee ages
        int[] ages = new int[count];

        // Prompt the user to input employee ages
        System.out.println("Enter the ages of " + count + " employees:");

        // Input employee ages into the array
        for (int i = 0; i < count; i++) {
            System.out.print("Enter age of employee " + (i + 1) + ": ");
            ages[i] = scanner.nextInt();
        }

        // Display the entered employee ages
        System.out.println("Employee ages:");
        for (int i = 0; i < count; i++) {
            System.out.println("Employee " + (i + 1) + ": " + ages[i]);
        }

        // Close the Scanner object
        scanner.close();
    }
}

