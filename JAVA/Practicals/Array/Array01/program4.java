import java.util.Scanner;

class SumOfOddElements {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        
        int[] arr = new int[size];

        
        System.out.println("Enter " + size + " elements:");

               for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            arr[i] = scanner.nextInt();
        }

        
        int sum = 0;
        for (int num : arr) {
            if (num % 2 != 0) {
                sum += num;
            }
        }

        
        System.out.println("Sum of odd elements in the array: " + sum);

            scanner.close();
    }
}

