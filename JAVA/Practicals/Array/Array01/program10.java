import java.util.Scanner;
class Array {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Define an array to store temperatures for each day of the week
        double[] temperatures = new double[7];
        String[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        // Prompt the user to input temperatures for each day
        System.out.println("Enter temperatures for each day of the week:");

        for (int i = 0; i < temperatures.length; i++) {
            System.out.print("Temperature for " + daysOfWeek[i] + ": ");
            temperatures[i] = scanner.nextDouble();
        }

        // Calculate the average temperature
        double sum = 0;
        for (double temp : temperatures) {
            sum += temp;
        }
        double averageTemperature = sum / temperatures.length;
        System.out.println("Average temperature for the week: " + averageTemperature);

        // Find the maximum temperature and the day it occurred
        double maxTemperature = temperatures[0];
        int maxTemperatureDayIndex = 0;
        for (int i = 1; i < temperatures.length; i++) {
            if (temperatures[i] > maxTemperature) {
                maxTemperature = temperatures[i];
                maxTemperatureDayIndex = i;
            }
        }
        System.out.println("Maximum temperature of the week: " + maxTemperature + " on " + daysOfWeek[maxTemperatureDayIndex]);

        // Close the Scanner object
        scanner.close();
    }
}

