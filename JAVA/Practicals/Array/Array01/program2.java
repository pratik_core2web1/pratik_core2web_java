import java.util.Scanner;
class Array{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

  
        int[] arr = new int[10];

  
        System.out.println("Enter 10 elements:");

  
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            arr[i] = scanner.nextInt();
        }

  
        System.out.print("Enter the index of the element to print (0-9): ");
        int index = scanner.nextInt();

  
        if (index >= 0 && index < arr.length) {
  
            System.out.println("Output: " + arr[index]);
        } else {
            System.out.println("Invalid index. Please enter an index between 0 and 9.");
        }

        scanner.close();
    }
}

