import java.util.Scanner;

class Array{
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Ask the user for the size of the array
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        // Define an array of the specified size
        int[] array = new int[size];

        // Prompt the user to input elements
        System.out.println("Enter " + size + " elements:");

        // Input elements into the array
        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        // Print elements divisible by 4
        System.out.print("Elements divisible by 4: ");
        for (int num : array) {
            if (num % 4 == 0) {
                System.out.print(num + " ");
            }
        }

        // Close the Scanner object
        scanner.close();
    }
}

