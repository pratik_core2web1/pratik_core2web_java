import java.util.Scanner;

class Array {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Ask the user for the size of the array
        System.out.print("Enter the size of the character array: ");
        int size = scanner.nextInt();

        // Define a character array of the specified size
        char[] charArray = new char[size];

        // Prompt the user to input characters
        System.out.println("Enter " + size + " characters:");

        // Input characters into the array
        for (int i = 0; i < size; i++) {
            System.out.print("Enter character " + (i + 1) + ": ");
            charArray[i] = scanner.next().charAt(0);
        }

        // Print characters in the array
        System.out.println("Characters in the array:");
        for (char ch : charArray) {
            System.out.print(ch + " ");
        }

        // Close the Scanner object
        scanner.close();
    }
}

