import java.util.HashSet;

class Array06{
    public static void main(String[] args) {
    
        int[] arr1 = {45, 67, 97, 87, 90, 80};
        int[] arr2 = {15, 97, 67, 80, 90, 10};


        HashSet<Integer> commonElements = new HashSet<>();
        for (int num1 : arr1) {
            for (int num2 : arr2) {
                if (num1 == num2) {
                    commonElements.add(num1);
                    break;
                }
            }
        }


        System.out.print("Common elements in the given arrays are: ");
        for (int num : commonElements) {
            System.out.print(num + ", ");
        }
    }
}

