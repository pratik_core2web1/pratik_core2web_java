import java.util.Arrays;

class Array06 {
    public static void main(String[] args) {
 
        int[] arr = {56, 15, 8, 26, 7, 50, 54};


        int thirdLargest = findThirdLargest(arr);


        System.out.println("Third largest element is: " + thirdLargest);
    }


    public static int findThirdLargest(int[] arr) {

        Arrays.sort(arr);


        int n = arr.length;
        int thirdLargestIndex = n - 3;


        return arr[thirdLargestIndex];
    }
}

