import java.util.Scanner;

class Array06{
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the size of the array
        System.out.print("Enter size: ");
        int size = scanner.nextInt();

        // Define the array
        char[] array = new char[size];

        // Prompt the user to enter array elements
        System.out.println("Enter array elements:");
        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            int num = scanner.nextInt();

            // Check if the number is in the range of ASCII value of A to Z
            if (num >= 'A' && num <= 'Z') {
                array[i] = (char) num;
            } else {
                array[i] = (char) num;
            }
        }

        // Print the array
        System.out.println("Array will be like:");
        System.out.print("arr1 ");
        for (char ch : array) {
            System.out.print(ch + " ");
        }

        // Close the Scanner object
        scanner.close();
    }
}

