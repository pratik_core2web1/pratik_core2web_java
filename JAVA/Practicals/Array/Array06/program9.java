import java.util.Scanner;

class Array{ 
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        int[] arr = {121, 1, 58, 333, 616, 9};


        int palindromeCount = 0;


        for (int num : arr) {
            if (isPalindrome(num)) {
                palindromeCount++;
            }
        }


        System.out.println("Count of palindrome elements is: " + palindromeCount);



    }


    public static boolean isPalindrome(int number) {
        int originalNumber = number;
        int reversedNumber = 0;
        while (number != 0) {
            int remainder = number % 10;
            reversedNumber = reversedNumber * 10 + remainder;
            number /= 10;
        }
        return originalNumber == reversedNumber;
    }
}

