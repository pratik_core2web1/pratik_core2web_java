import java.util.Scanner;

class Array06 {
    public static void main(String[] args) {
        // Define the array
        int[] array = {5, 10, 16, 20, 25, 30, 35, 4, 8, 12, 16, 20};

        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the key
        System.out.print("Enter key: ");
        int key = scanner.nextInt();

        // Initialize a flag to check if at least one element is found
        boolean found = false;

        // Iterate through the array to check for multiples of the key
        for (int i = 0; i < array.length; i++) {
            if (array[i] % key == 0) {
                System.out.println("An element multiple of " + key + " found at index: " + i);
                found = true;
            }
        }

        // If no element is found, print "Element Not Found."
        if (!found) {
            System.out.println("Element Not Found.");
        }

        // Close the Scanner object
        scanner.close();
    }
}

