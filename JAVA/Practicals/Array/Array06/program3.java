import java.util.Scanner;

class Array06 {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Define the array
        int[] arr = {11, 6, 8, 9, 5, 8, 7, 8, 6};

        // Prompt the user to enter the key
        System.out.print("Enter key: ");
        int key = scanner.nextInt();

        // Count the occurrences of the key in the array
        int count = 0;
        for (int num : arr) {
            if (num == key) {
                count++;
            }
        }

        // Replace the elements with their cube if the count is more than 2
        if (count > 2) {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == key) {
                    arr[i] = key * key * key;
                }
            }

            // Print the modified array
            System.out.println("Array will be like:");
            for (int num : arr) {
                System.out.print(num + " ");
            }
        } else {
            // Print Element Not Found if the key is not present more than 2 times
            System.out.println("Element Not Found.");
        }

        
   
    }
}

