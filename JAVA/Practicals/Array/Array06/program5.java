import java.util.Arrays;

class Array06{
    public static void main(String[] args) {
        // Define the arrays
        int[] arr1 = {5, 10, 15, 20, 25, 30, 35};
        int[] arr2 = {4, 8, 12, 16, 20};

        // Calculate the size of the merged array
        int mergedSize = arr1.length + arr2.length;

        // Define the merged array
        int[] mergedArray = new int[mergedSize];

        // Copy elements from arr1 to mergedArray
        System.arraycopy(arr1, 0, mergedArray, 0, arr1.length);

        // Copy elements from arr2 to mergedArray
        System.arraycopy(arr2, 0, mergedArray, arr1.length, arr2.length);

        // Print the merged array
        System.out.println("Array after merger:");
        System.out.println(Arrays.toString(mergedArray));
    }
}

