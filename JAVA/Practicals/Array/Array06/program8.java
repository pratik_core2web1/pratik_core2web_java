import java.util.Scanner;

class Array06 {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the size of the array
        System.out.print("Size = ");
        int size = scanner.nextInt();

        // Define the character array
        char[] array = new char[size];

        // Prompt the user to enter array elements
        System.out.println("Enter array elements:");
        for (int i = 0; i < size; i++) {
            System.out.print((char) ('A' + i) + " ");
            array[i] = (char) ('A' + i);
        }

        // Print alternate elements before reverse
        System.out.println("\n\nBefore Reverse:");
        printAlternate(array);

        // Reverse the array
        reverseArray(array);

        // Print alternate elements after reverse
        System.out.println("\n\nAfter Reverse:");
        printAlternate(array);

        // Close the Scanner object
        scanner.close();
    }

    // Function to reverse a character array
    public static void reverseArray(char[] array) {
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            char temp = array[left];
            array[left] = array[right];
            array[right] = temp;
            left++;
            right--;
        }
    }

    // Function to print alternate elements of a character array
    public static void printAlternate(char[] array) {
        for (int i = 0; i < array.length; i += 2) {
            System.out.print(array[i] + " ");
        }
    }
}

