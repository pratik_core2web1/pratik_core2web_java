import java.util.Scanner;

class Array02{
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Ask the user for the size of the array
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        // Define an array of the specified size
        int[] array = new int[size];

        // Prompt the user to input elements
        System.out.println("Enter " + size + " integers:");

        // Input elements into the array
        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        // Print the array
        System.out.println("Array elements are:");
        if (size % 2 == 0) {
            // If the size is even, print alternate elements
            for (int i = 0; i < size; i += 2) {
                System.out.println(array[i]);
            }
        } else {
            // If the size is odd, print the whole array
            for (int num : array) {
                System.out.println(num);
            }
        }

        // Close the Scanner object
        scanner.close();
    }
}

