import java.util.Scanner;

class Array02 {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        
        int[] array = new int[size];

        System.out.println("Enter " + size + " integers:");

        
        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        
        System.out.print("Enter the number to search in array: ");
        int numberToSearch = scanner.nextInt();

             int foundIndex = -1;
        for (int i = 0; i < size; i++) {
            if (array[i] == numberToSearch) {
                foundIndex = i;
                break;
            }
        }

        // Print the result
        if (foundIndex != -1) {
            System.out.println("The number " + numberToSearch + " is found at index " + foundIndex);
        } else {
            System.out.println("The number " + numberToSearch + " is not found in the array");
        }

    }
}

