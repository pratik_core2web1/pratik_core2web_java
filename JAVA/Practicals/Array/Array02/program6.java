import java.util.Scanner;

class Array02 {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Ask the user for the size of the array
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        // Define an array of the specified size
        int[] array = new int[size];

        // Prompt the user to input elements
        System.out.println("Enter " + size + " integers:");

        // Input elements into the array
        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        // Calculate the product of the odd-indexed elements
        int product = 1;
        for (int i = 1; i < size; i += 2) {
            product *= array[i];
        }

        // Print the product of the odd-indexed elements
        System.out.println("Product of odd indexed elements: " + product);

        // Close the Scanner object
        scanner.close();
    }
}

