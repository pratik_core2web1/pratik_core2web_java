import java.util.Scanner;

class Array02 {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);

         System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        
        int[] array = new int[size];

        
        System.out.println("Enter " + size + " integers:");

             for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        
        System.out.println("Even numbers in the array:");
        int evenCount = 0;
        for (int num : array) {
            if (num % 2 == 0) {
                System.out.print(num + " ");
                evenCount++;
            }
        }
        System.out.println("\nTotal even numbers in the array: " + evenCount);

      
    }
}

