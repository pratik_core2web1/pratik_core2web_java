import java.util.Scanner;
class Array02 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        int[] array = new int[size];

      System.out.println("Enter " + size + " integers:");


        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        int max = array[0];
        int maxIndex = 0;
        for (int i = 1; i < size; i++) {
            if (array[i] > max) {
                max = array[i];
                maxIndex = i;
            }
        }

       System.out.println("Maximum number in the array is found at pos " + (maxIndex + 1) + " is " + max);


    }
}

