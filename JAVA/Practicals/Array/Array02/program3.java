import java.util.Scanner;
class Array02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Enter the size of the character array: ");
        int size = scanner.nextInt();

        
        char[] charArray = new char[size];

        
        System.out.println("Enter " + size + " characters:");

              for (int i = 0; i < size; i++) {
            System.out.print("Enter character " + (i + 1) + ": ");
            charArray[i] = scanner.next().charAt(0);
        }

        // Check if there is any vowel in the array and print its index
        System.out.println("Output:");
        for (int i = 0; i < size; i++) {
            if (isVowel(charArray[i])) {
                System.out.println("vowel " + charArray[i] + " found at index " + i);
            }
        }

        
    }

    // Function to check if a character is a vowel
    public static boolean isVowel(char ch) {
        ch = Character.toLowerCase(ch);
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }
}

