import java.util.Scanner;

class Array02{
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Ask the user for the size of the array
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        // Define an array of the specified size
        int[] array = new int[size];

        // Prompt the user to input elements
        System.out.println("Enter " + size + " integers:");

        // Input elements into the array
        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        // Print elements greater than 5 but less than 9
        System.out.println("Output:");
        for (int num : array) {
            if (num > 5 && num < 9) {
                System.out.println(num + " is greater than 5 but less than 9");
            }
        }

        // Close the Scanner object
        scanner.close();
    }
}

