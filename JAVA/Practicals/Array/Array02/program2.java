import java.util.Scanner;

class Array02 {
    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

    
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

    
        int[] array = new int[size];

    
        System.out.println("Enter " + size + " integers:");

    
        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        System.out.println("Numbers divisible by 3 in the array:");
        int divisibleByThreeSum = 0;
        for (int num : array) {
            if (num % 3 == 0) {
                System.out.print(num + " ");
                divisibleByThreeSum += num;
            }
        }
        System.out.println("\nSum of numbers divisible by 3 in the array: " + divisibleByThreeSum);

    
    }
}

