import java.util.Scanner;

class Array{
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        // Define an array of the specified size
        int[] array = new int[size];

        // Prompt the user to input elements
        System.out.println("Enter " + size + " integers:");

        // Input elements into the array
        for (int i = 0; i < size; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        // Calculate the sum of the odd-indexed elements
        int sum = 0;
        for (int i = 1; i < size; i += 2) {
            sum += array[i];
        }

        // Print the sum of the odd-indexed elements
        System.out.println("Sum of odd indexed elements: " + sum);

            }
}

