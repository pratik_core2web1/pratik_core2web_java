import java.util.Scanner;

class Array {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);

      
        System.out.print("Enter the size: ");
        int size = scanner.nextInt();

  
        int[] array = new int[size];


        System.out.println("Enter the elements of the array:");

       
        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

    
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        for (int num : array) {
            if (num > max) {
                secondMax = max;
                max = num;
            } else if (num > secondMax && num != max) {
                secondMax = num;
            }
        }


        System.out.println("The second largest element in the array is: " + secondMax);



    }
}

