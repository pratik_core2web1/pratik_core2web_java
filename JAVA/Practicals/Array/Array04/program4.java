import java.util.Scanner;

class Array04 {
   public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();


        int[] array = new int[size];


        System.out.println("Enter the elements of the array:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        System.out.print("Enter the number to check: ");
        int numberToCheck = scanner.nextInt();


        int count = 0;
        for (int num : array) {
            if (num == numberToCheck) {
                count++;
            }
        }

  
        if (count > 2) {
            System.out.println(numberToCheck + " occurs more than 2 times in the array.");
        } else if (count == 2) {
            System.out.println(numberToCheck + " occurs 2 times in the array.");
        } else {
            System.out.println(numberToCheck + " does not occur 2 times in the array.");
        }

        
    
    }
}

