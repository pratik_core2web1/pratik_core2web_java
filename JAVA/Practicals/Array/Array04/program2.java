import java.util.Scanner;

class Array04{
    public static void main(String[] args) {
      
        Scanner scanner = new Scanner(System.in);

     
        System.out.print("Enter the size: ");
        int size = scanner.nextInt();

   
        int[] array = new int[size];

 
        System.out.println("Enter the elements of the array:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        int min = array[0];
        for (int i = 1; i < size; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }


        int max = array[0];
        for (int i = 1; i < size; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        int difference = max - min;
        System.out.println("The difference between the minimum and maximum elements is: " + difference);
    }
}

