import java.util.Scanner;

class Array04 {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);

     
        System.out.print("Enter the size: ");
        int size = scanner.nextInt();

   
        int[] array = new int[size];


        System.out.println("Enter array elements:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        int sum = 0;
        for (int num : array) {
            sum += num;
        }


        double average = (double) sum / size;


        System.out.println("Array elements' average is: " + (int) average);



    }
}

