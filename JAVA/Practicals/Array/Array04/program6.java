import java.util.Scanner;

class Array04 {
    public static void main(String[] args) {
    
        Scanner scanner = new Scanner(System.in);

   
       System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();


        char[] array = new char[size];


        System.out.println("Enter the elements of the array:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.next().charAt(0); // Read only the first character
        }


        int vowelCount = 0;
        int consonantCount = 0;
        for (char ch : array) {
            if (isVowel(ch)) {
                vowelCount++;
            } else if (Character.isLetter(ch)) {
                consonantCount++;
            }
        }


        System.out.println("Output:");
        System.out.println("Count of vowels: " + vowelCount);
        System.out.println("Count of consonants: " + consonantCount);



    }


    public static boolean isVowel(char ch) {
        ch = Character.toLowerCase(ch);
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }
}

