import java.util.Scanner;

class ReverseArray {
    public static void main(String[] args) {
     
        Scanner scanner = new Scanner(System.in);

    
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();


        int[] array = new int[size];


        System.out.println("Enter the elements of the array:");


        for (int i = 0; i < size; i++) {
           System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        int start = 0;
        int end = size - 1;
        while (start < end) {

            int temp = array[start];
            array[start] = array[end];
            array[end] = temp;
            start++;
            end--;
        }


        System.out.println("Reversed array:");
        for (int num : array) {
            System.out.println(num);
        }


    }
}

