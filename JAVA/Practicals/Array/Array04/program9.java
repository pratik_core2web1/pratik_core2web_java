import java.util.Scanner;

class Array04 {
    public static void main(String[] args) {
    
        Scanner scanner = new Scanner(System.in);

   
        System.out.print("Enter the size: ");
        int size = scanner.nextInt();


        char[] array = new char[size];


        System.out.println("Enter Elements:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.next().charAt(0);
        }


        for (int i = 0; i < size; i++) {
            if (array[i] < 'a' || array[i] > 'z') {
                array[i] = '#';
            }
        }


        System.out.println("Output:");
        for (char ch : array) {
            System.out.println(ch);
        }

        
   
    }
}

