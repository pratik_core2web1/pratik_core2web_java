import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        

        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        System.out.print("Enter number of columns: ");
        int columns = scanner.nextInt();


        if (rows != columns) {
            System.out.println("Number of rows and columns must be equal to form a square matrix.");
            return;
        }
        

        int[][] array = new int[rows][columns];
        

        System.out.println("Enter the elements of the array row by row:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print("Enter element at position (" + i + "," + j + "): ");
                array[i][j] = scanner.nextInt();
            }
        }
        
        // Calculating the sum of the secondary diagonal
        int sum = 0;
        for (int i = 0; i < rows; i++) {
            sum += array[i][columns - 1 - i];
        }
        

        System.out.println("Sum of Secondary Diagonal: " + sum);
        
  }
}

