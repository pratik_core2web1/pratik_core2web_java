import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Taking the size of the array from the user
        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        System.out.print("Enter number of columns: ");
        int columns = scanner.nextInt();

        // Ensuring there are at least 2 rows and 2 columns for corner elements
        if (rows < 2 || columns < 2) {
            System.out.println("Number of rows and columns must be at least 2 to have corner elements.");
            return;
        }
        
        // Initializing the 2D array
        int[][] array = new int[rows][columns];
        
        // Taking array elements from the user
        System.out.println("Enter the elements of the array row by row:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print("Enter element at position (" + i + "," + j + "): ");
                array[i][j] = scanner.nextInt();
            }
        }
        
        // Printing the corner elements
        System.out.print("Output: ");
        System.out.print(array[0][0] + " "); // Top-left corner
        System.out.print(array[0][columns - 1] + " "); // Top-right corner
        System.out.print(array[rows - 1][0] + " "); // Bottom-left corner
        System.out.print(array[rows - 1][columns - 1]); // Bottom-right corner
        System.out.println();
        

    }
}

