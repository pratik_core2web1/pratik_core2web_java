import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Taking the size of the array from the user
        System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        System.out.print("Enter number of columns: ");
        int columns = scanner.nextInt();

        // Ensuring the array is square for the diagonals to be well-defined
        if (rows != columns) {
            System.out.println("Number of rows and columns must be equal to form a square matrix.");
            return;
        }
        
        // Initializing the 2D array
        int[][] array = new int[rows][columns];
        
        // Taking array elements from the user
        System.out.println("Enter the elements of the array row by row:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print("Enter element at position (" + i + "," + j + "): ");
                array[i][j] = scanner.nextInt();
            }
        }
        
        // Calculating the sum of the primary diagonal
        int primaryDiagonalSum = 0;
        for (int i = 0; i < rows; i++) {
            primaryDiagonalSum += array[i][i];
        }

        // Calculating the sum of the secondary diagonal
        int secondaryDiagonalSum = 0;
        for (int i = 0; i < rows; i++) {
            secondaryDiagonalSum += array[i][columns - 1 - i];
        }

        // Calculating the product of the sums of the primary and secondary diagonals
        int product = primaryDiagonalSum * secondaryDiagonalSum;
        
        // Printing the product
        System.out.println("Product of Sum of Primary and Secondary Diagonal: " + product);
        

    }
}

