import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        

       System.out.print("Enter number of rows: ");
        int rows = scanner.nextInt();
        System.out.print("Enter number of columns: ");
        int columns = scanner.nextInt();


        if (rows != columns) {
            System.out.println("Number of rows and columns must be equal to form a square matrix.");
            return;
        }
        

        int[][] array = new int[rows][columns];
        

        System.out.println("Enter the elements of the array row by row:");
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print("Enter element at position (" + i + "," + j + "): ");
                array[i][j] = scanner.nextInt();
            }
        }
        

        int product = 1;
        for (int i = 0; i < rows; i++) {
            product *= array[i][i];
        }
        

        System.out.println("Product of Primary Diagonal: " + product);
        

    }
}

