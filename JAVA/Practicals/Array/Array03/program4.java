class  Array03{
    public static void main(String[] args) {
        
        int[] array = {1, 2, 3, 12, 15, 6, 7, 10, 9};

       
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                array[i] = 0;
            } else {
                array[i] = 1;
            }
        }

      
        System.out.println("Output:");
        for (int num : array) {
            System.out.print(num + " ");
        }
    }
}

