import java.util.Scanner;

class Array03 {
    public static void main(String[] args) {
        
        int[] array = {2, 5, 2, 7, 8, 9, 2};

      
        Scanner scanner = new Scanner(System.in);

     
        System.out.print("Specific number: ");
        int specificNumber = scanner.nextInt();

    
        int count = 0;
        for (int num : array) {
            if (num == specificNumber) {
                count++;
            }
        }

 
        if (count > 0) {
            System.out.println("Number " + specificNumber + " occurred " + count + " times in an array.");
        } else {
            System.out.println("Number " + specificNumber + " not found in array.");
        }


        scanner.close();
    }
}

