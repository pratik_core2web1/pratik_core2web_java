class Array03 {
    public static void main(String[] args) {
        
        int[] array = {10, 20, 30, 40, 50, 60};

       
        for (int i = 0; i < array.length; i++) {
            array[i] += 15;
        }

   
        System.out.println("Output:");
        for (int num : array) {
            System.out.print(num + " ");
        }
    }
}

