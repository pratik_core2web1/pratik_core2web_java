import java.util.Scanner;

class Array03 {
    public static void main(String[] args) {
        
        int[] array = {1, 5, 9, 8, 7, 6};

       
        Scanner scanner = new Scanner(System.in);

     
        System.out.print("Specific number: ");
        int specificNumber = scanner.nextInt();

    
        int firstIndex = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == specificNumber) {
                firstIndex = i;
                break;
            }
        }

 
        if (firstIndex != -1) {
            System.out.println("Number " + specificNumber + " first occurred at index: " + firstIndex);
        } else {
            System.out.println("Number " + specificNumber + " not found in array.");
        }



    }
}

