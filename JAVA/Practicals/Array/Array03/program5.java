class  Array03{
    public static void main(String[] args) {
        
        int[] array = {-2, 5, -6, 7, -3, 8};

       
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                array[i] *= array[i]; // Square the negative number
            }
        }

   
        System.out.println("Output:");
        for (int num : array) {
            System.out.print(num + " ");
        }
    }
}

