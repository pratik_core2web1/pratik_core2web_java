import java.util.Scanner;

class Array03 {
    public static void main(String[] args) {
       
        Scanner scanner = new Scanner(System.in);

      
        System.out.print("Size: ");
        int size = scanner.nextInt();


        int[] array = new int[size];


        System.out.println("Enter " + size + " integers:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        System.out.println("Output:");
        if (size % 2 != 0 && size >= 5) {

            for (int num : array) {
                if (num % 2 != 0) {
                    System.out.print(num + " ");
                }
            }
        } else {

            for (int num : array) {
                if (num % 2 == 0) {
                    System.out.print(num + " ");
                }
            }
        }



    }
}

