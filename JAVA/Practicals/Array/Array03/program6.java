class Array03{
    public static void main(String[] args) {
       
        char[] array = {'a', 'b', 'c', 'y', 'x', 'o', 'p'};

      
        System.out.println("Output:");
        for (char ch : array) {
            if (!isVowel(ch)) {
                System.out.print(ch + " ");
            }
        }
    }

    
    public static boolean isVowel(char ch) {
        ch = Character.toLowerCase(ch);
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }
}

