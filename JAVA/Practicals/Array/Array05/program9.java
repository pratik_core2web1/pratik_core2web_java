import java.util.Scanner;

class Array05 {
    public static void main(String[] args) {
       
        Scanner scanner = new Scanner(System.in);

      
        System.out.print("Enter the Number: ");
        long number = scanner.nextLong();

     
        scanner.close();

   
        String numberString = Long.toString(number);


        int[] incrementedDigits = new int[numberString.length()];


        for (int i = 0; i < numberString.length(); i++) {
            int digit = Character.getNumericValue(numberString.charAt(i));
            incrementedDigits[i] = digit + 1;
        }


        System.out.println("Output:");
        for (int digit : incrementedDigits) {
            System.out.print(digit + ", ");
        }
    }
}

