import java.util.Scanner;

class Array05{
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);

       
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

   
        int[] array = new int[size];


        System.out.println("Enter the elements of the array:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        int oddSum = 0;
        int evenSum = 0;
        for (int num : array) {
            if (num % 2 == 0) {
                evenSum += num;
            } else {
                oddSum += num;
            }
        }


        System.out.println("Output:");
        System.out.println("Odd Sum = " + oddSum);
        System.out.println("Even Sum = " + evenSum);



    }
}

