import java.util.Scanner;

class Array05{
    public static void main(String[] args) {
       
        Scanner scanner = new Scanner(System.in);

     
        System.out.print("Enter the size: ");
        int size = scanner.nextInt();

  
        int[] array = new int[size];


        System.out.println("Enter the elements of the array:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        int min = Integer.MAX_VALUE;
        int secondMin = Integer.MAX_VALUE;
        for (int num : array) {
            if (num < min) {
                secondMin = min;
                min = num;
            } else if (num < secondMin && num != min) {
                secondMin = num;
            }
        }

        // Print the result
        if (secondMin == Integer.MAX_VALUE) {
            System.out.println("No second minimum element found in the array.");
        } else {
            System.out.println("The second minimum element in the array is: " + secondMin);
        }



    }
}

