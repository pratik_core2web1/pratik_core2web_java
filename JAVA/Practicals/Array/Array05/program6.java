import java.util.Scanner;

class Array05{
    public static void main(String[] args) {
  
        Scanner scanner = new Scanner(System.in);


        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();


        int[] array = new int[size];


        System.out.println("Enter the elements of the array:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        int firstPrimeIndex = -1;
        for (int i = 0; i < size; i++) {
            if (isPrime(array[i])) {
                firstPrimeIndex = i;
                break;
            }
        }


        if (firstPrimeIndex != -1) {
            System.out.println("First prime number found at index " + firstPrimeIndex);
        } else {
            System.out.println("No prime number found in the array.");
        }



    }


    public static boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}

