import java.util.Scanner;

class Array05{
    public static void main(String[] args) {
       
        Scanner scanner = new Scanner(System.in);

   
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

  
        int[] array = new int[size];


        System.out.println("Enter the elements of the array:");


        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }


        System.out.println("Output:");
        for (int num : array) {
            System.out.print(factorial(num) + ", ");
        }



    }


    public static long factorial(int number) {
        long result = 1;
        for (int i = 2; i <= number; i++) {
            result *= i;
        }
        return result;
    }
}

