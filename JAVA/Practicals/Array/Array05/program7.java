import java.util.Scanner;

class Array05 {
    public static void main(String[] args) {
        // Create a Scanner object to take user input
        Scanner scanner = new Scanner(System.in);

        // Ask the user for the size of the array
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();

        // Define an array of the specified size
        int[] array = new int[size];

        // Prompt the user to input elements
        System.out.println("Enter the elements of the array:");

        // Input elements into the array
        for (int i = 0; i < size; i++) {
            System.out.print("Element " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        // Print the composite numbers in the array
        System.out.print("Composite numbers in an array are: ");
        for (int num : array) {
            if (!isPrime(num)) {
                System.out.print(num + ", ");
            }
        }

        
     
    }

  
    public static boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}

