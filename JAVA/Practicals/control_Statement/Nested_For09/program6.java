import java.util.Scanner;

public class Pattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Get the number of rows from the user
        System.out.println("Enter the number of rows: ");
        int rows = scanner.nextInt();

        // Print the pattern
        for (int i = 0; i < rows; i++) {
            // Print each character in reverse order
            for (char ch = 'd'; ch >='a' ; ch--) {
                System.out.print(ch + " ");
            }
            // Move to the next line after printing each row
            System.out.println();
        }
    }
}

