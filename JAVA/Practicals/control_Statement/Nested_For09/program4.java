class Pattern {
    public static void main(String[] args) {
        int rows = 3;
        char currentChar = 'A';

        // Iterate over the rows
        for (int i = 0; i < rows; i++) {
            // Print each character in the row
            for (int j = 0; j < 3; j++) {
                System.out.print(currentChar + " ");
                currentChar += 2; // Increment by 2 to get the next character
            }
            // Move to the next line after printing each row
            System.out.println();
        }
    }
}

