import java.util.Scanner;

class Pattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Get the number of rows from the user
        System.out.println("Enter the number of rows: ");
        int rows = scanner.nextInt();

        // Print the pattern
        for (int i = 0; i < rows; i++) {
            // Print each row
            for (int j = 0; j <rows;j++) {
                System.out.print("1A ");
            }
            System.out.println(); // Move to the next line after printing each row
        }
    }
}
