/*row from user 3
c1 c2 c3
c4 c5 c5
c7 c8 c9*/

import java.util.Scanner;

public class Pattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Get the number of rows from the user
        System.out.println("Enter the number of rows: ");
        int rows = scanner.nextInt();

        int count = 1;

        // Print the pattern
        for (int i = 0; i < rows; i++) {
            // Print each character in the row
            for (int j = 0; j < 3; j++) {
                System.out.print("c" + count++ + " ");
            }
            // Move to the next line after printing each row
            System.out.println();
        }
    }
}

