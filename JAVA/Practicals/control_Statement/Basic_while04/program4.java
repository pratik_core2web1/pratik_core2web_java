class Demo{ 
    public static void main(String[] args) {
        System.out.println("Squares of the first 10 natural numbers in reverse order:");

        int number = 10;

        // Iterate while number is greater than 0
        while (number > 0) {
            int square = number * number; // Calculate square of the current number
            System.out.println(square);
            number--; // Decrement number for the next iteration
        }
    }
}

