class Demo{
    public static void main(String[] args) {
        System.out.println("Numbers divisible by 5 in the range 50 to 10:");

           for (int i = 50; i >= 10; i--) {
               if (i % 5 == 0) {
                System.out.println(i);
            }
        }
    }
}
