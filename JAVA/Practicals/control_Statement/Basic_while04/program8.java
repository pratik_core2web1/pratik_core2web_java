class Demo{
    public static void main(String[] args) {
        int sum = 0;

        // Iterate from 90 down to 11
        for (int i = 90; i >= 11; i--) {
            sum += i; // Add current number to sum
        }

        System.out.println("Sum of integers from 90 to 11: " + sum);
    }
}
