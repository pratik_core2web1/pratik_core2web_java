class Demo{
    public static void main(String[] args) {
        System.out.println("Consonant alphabets from A to Z:");

        // Iterate over the alphabets from A to Z
        for (char ch = 'A'; ch <= 'Z'; ch++) {
            // Check if the current character is not a vowel
            if (!isVowel(ch)) {
                System.out.println(ch + " ");
            }
        }
    }

    // Method to check if a character is a vowel
    public static boolean isVowel(char ch) {
        ch = Character.toUpperCase(ch);
        return ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U';
    }
}

