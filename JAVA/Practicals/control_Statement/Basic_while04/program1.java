class PrintAsciiInRange {
    public static void main(String[] args) {
     
        int start = 90;
        int end = 65;

        System.out.println("ASCII values in range 90 to 65:");

        for (int i = start; i >= end; i--) {
            System.out.println((char)i + "' is " + i);
        }
    }
}
