class Demo{

    public static void main(String[] args) {
        System.out.println("Cubes of the first 10 natural numbers:");

        // Iterate over the first 10 natural numbers
        for (int i = 1; i <= 10; i++) {
            int cube = i * i * i; // Calculate the cube of the current number
            System.out.println("Cube of " + i + " = " + cube);
        }
    }
}
