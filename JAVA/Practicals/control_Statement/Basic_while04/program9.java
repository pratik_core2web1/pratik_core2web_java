class Demo{
    	public static void main(String[] args) {
        int sum = 0;

        // Iterate from 150 down to 101
        for (int i = 150; i >= 101; i--) {
            // Check if the number is odd
            if (i % 2 != 0) {
                sum += i; // Add the odd number to the sum
            }
        }

        System.out.println("Sum of odd numbers from 150 to 101: " + sum);
    }
}
