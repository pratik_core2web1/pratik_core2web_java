import java.util.Scanner;

public class Pattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Get the number of rows from the user
        System.out.println("Enter the number of rows: ");
        int rows = scanner.nextInt();
        scanner.close();

        char ch = 'A'; // Initialize the starting character
        
        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print characters based on the row number
            for (int j = 1; j <= i; j++) {
                System.out.print(ch++);
            }
            // Move to the next line after printing each row
            System.out.println();
            ch = (char) (ch - i + 2); // Move to the next character for the next row
        }
    }
}

