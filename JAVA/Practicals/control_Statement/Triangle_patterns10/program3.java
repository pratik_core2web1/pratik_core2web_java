import java.util.Scanner;

public class Pattern {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Get the number of rows from the user
        System.out.println("Enter the number of rows: ");
        int rows = scanner.nextInt();

        // Print the pattern
        for (int i = 1; i <= rows; i++) {
            // Print numbers from 'rows' down to 'rows - i + 1' for each row
            for (int j = rows; j >= rows - i + 1; j--) {
                System.out.print(j);
            }
            // Move to the next line after printing each row
            System.out.println();
        }
    }
}


