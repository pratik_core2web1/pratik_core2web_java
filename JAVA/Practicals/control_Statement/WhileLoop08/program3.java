class CharLoop {
    public static void main(String[] args) {
        // Iterate from 1 to 6
        for (int i = 1; i <= 6; i++) {
            // Convert integer value to corresponding character ('A' + i - 1)
            char ch = (char) ('A' + i - 1);
            System.out.print(ch + " ");
        }
    }
}

