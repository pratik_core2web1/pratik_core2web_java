class OddSquareNumbers {
    public static void main(String[] args) {
        System.out.println("Numbers from 150 to 198 whose square is odd:");

        // Iterate from 150 to 198
        int start = 150;
        int end = 198;

        while (start <= end) {
            // Check if the square of the current number is odd
            if ((start * start) % 2 != 0) {
                System.out.println(start);
            }
            start++;
        }
    }
}
