class IncrementByTwo {
    public static void main(String[] args) {
        int num = 1;

        System.out.println("Numbers from 1 to 100 incremented by 2:");

        // Using a while loop to print numbers from 1 to 100 incrementing by 2
        while (num <= 100) {
            System.out.print(num);
            num += 2;
        }
    }
}
