class OddDigits {
    public static void main(String[] args) {
        long number = 216985;

        System.out.print("Odd digits in " + number + ": ");

        // Iterate through each digit of the number
        while (number != 0) {
            long digit = number % 10; // Extract the last digit
            if (digit % 2 != 0) { // Check if the digit is odd
                System.out.print(digit + " "); // Print the odd digit
            }
            number = number / 10; // Remove the last digit
        }
    }
}

