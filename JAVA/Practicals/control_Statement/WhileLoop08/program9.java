class CountOddEvenDigits {
    public static void main(String[] args) {
        long number = 214367689;
        
        int evenCount = 0;
        int oddCount = 0;
        
        // Convert number to a string for easier manipulation
        String numberStr = Long.toString(number);

        // Iterate through each digit of the number
        for (int i = 0; i < numberStr.length(); i++) {
            // Get the digit at index i
            char digitChar = numberStr.charAt(i);
            // Convert digit character to integer
            int digit = Character.getNumericValue(digitChar);
            // Check if the digit is even or odd
            if (digit % 2 == 0) {
                evenCount++;
            } else {
                oddCount++;
            }
        }

        System.out.println("Number of even digits: " + evenCount);
        System.out.println("Number of odd digits: " + oddCount);
    }
}

