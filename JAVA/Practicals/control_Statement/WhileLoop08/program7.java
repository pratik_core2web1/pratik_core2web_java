class CountDigits {
    public static void main(String[] args) {
        long number = 93079224;
        int count = 0;

        // Count the number of digits using a while loop
        while (number != 0) {
            number = number / 10; // Remove the last digit
            count++; // Increment the count of digits
        }

        System.out.println("Number of digits in 93079224: " + count);
    }
}

