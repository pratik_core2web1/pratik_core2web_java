class CharLoop {
    public static void main(String[] args) {
        // Iterate from 1 to 6
        for (int i = 1; i <= 6; i++) {
            // Check if the number is odd
            if (i % 2 != 0) {
                // Convert odd number to corresponding character ('A' + (i - 1) / 2)
                char ch = (char) ('A' + (i - 1) / 2);
                System.out.print(ch + " ");
            } else {
                // Print the even number itself
                System.out.print(i + " ");
            }
        }
    }
}

