class OddNumberPattern {
    public static void main(String[] args) {
        int row = 4; // Number of rows
        int startNumber = 1; // Initial odd number

        // Iterate for the given number of rows
        for (int r = 0; r < row; r++) {
            // Iterate for each element in the row
            for (int i = 0; i < row; i++) {
                System.out.print(startNumber + " ");
                startNumber += 2; // Move to the next odd number
            }
            System.out.println(); // Move to the next line for the next row
        }
    }
}

