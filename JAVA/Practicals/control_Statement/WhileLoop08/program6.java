class ReverseNumber {
    public static void main(String[] args) {
        int number = 9307;
        int reversedNumber = 0;

        System.out.print("Original number: " + number + "\n");

        // Reverse the number using a while loop
        while (number != 0) {
            int digit = number % 10; // Extract the last digit
            reversedNumber = reversedNumber * 10 + digit; // Add the digit to the reversed number
            number = number / 10; // Remove the last digit from the original number
        }

        System.out.println("Reversed number: " + reversedNumber + "\n");
    }
}

