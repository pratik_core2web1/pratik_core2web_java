public class SumOfDigits {
    public static void main(String[] args) {
        long number = 9307922405L;
        long originalNumber = number; // Store the original number
        long sum = 0;

        // Iterate through each digit of the number
        while (number != 0) {
            long digit = number % 10; // Extract the last digit
            sum += digit; // Add the digit to the sum
            number /= 10; // Remove the last digit
        }

        System.out.println("Original number: " + originalNumber);
        System.out.println("Sum of digits: " + sum);
    }
}

